@extends('layouts.container')
@section('title', "The things we do in a nutshell")
@section('content')
    <!--begin landing page section-->
    <section class="hero is-fullheight">
        <div class="container is-fluid">
            <div class="hero-body">
                <div class="container has-text-centered">
                    <div class="columns is-multiline">
                        <div class="column is-4">
                            <nav class="panel title-space">
                                <p class="panel-heading">
                                    Our Services
                                </p>
                                <a href="{{ route('services') }}" class="panel-block">
                                    Mobile App Development
                                </a>
                                <a href="{{ route('portals') }}" class="panel-block is-active">
                                    Web Portals
                                </a>
                                <a href="{{ route('mobile-games') }}" class="panel-block">
                                    Mobile Game Development
                                </a>
                                <a href="{{ route('software') }}" class="panel-block">
                                    Custom Software Development
                                </a>
                                <a href="{{ route('networking') }}" class="panel-block">
                                    Networking
                                </a>
                                <a href="{{ route('graphics') }}" class="panel-block">
                                    Graphic Design
                                </a>
                            </nav>
                        </div>
                        <div class="column is-8">
                            <article class="media title-space mobile-app">
                                <div class="media-content has-text-justify">
                                    <figure class="image">
                                        <img src="{{ URL::asset('img/web.jpg') }}" class="is-square" alt="">
                                        <div class="caption">
                                            <p class="title is-4">Website Development</p>
                                            {{--<p class="subtitle">Something like a tag line of some sort</p>--}}
                                        </div>
                                    </figure>
                                    <div class="content title-space">
                                        <p>We provide Web designing (UI/UX), development, integration, security audit and maintenance. We also re-design the existing web portal look and feel (UI/UX). We migrate your current website to a modern website without any Downtime. We listen to what content and design you need, then we deliver in time at a minimum budget.</p>
                                        <p>We enable clients to lessen the often disastrous risks of content migration and heavy websites. Our services cover the <strong>full cycle of website redesign</strong> to help you:</p>
                                        <ul>
                                            <li>Create a new, modern and responsive website design.</li>
                                            <li>Mobile view responsive website.</li>
                                            <li>Improve content personalization to increase visitor engagement.</li>
                                            <li>Creating new functionality that would support your current business needs.</li>
                                            <li>Gather multiple websites under one roof with the help of a single CMS.</li>
                                            <li>Fix user interface bugs and low performance issue.</li>

                                        </ul>
                                        <p>While performing website redesign, we not only just replace the old technologies with the new ones, but we also anticipate your future business growth and ensure regular website updates.</p>
                                        <p>Click on the tab below to get a free project estimate by writing what you require from us and we will get in touch with you</p>
                                        <div class="has-text-right">
                                            <a class="button is-primary is-medium" href="{{ route('quote') }}">
                                            <span class="icon has-icons-left">
                                                <i class="fa fa-comments"></i>
                                            </span>
                                                <span>Get A quote</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--begin footer-->
@endsection