@extends('layouts.container')
@section('title', "The things we do in a nutshell")
@section('content')
    <!--begin landing page section-->
    <section class="hero is-fullheight">
        <div class="container is-fluid">
            <div class="hero-body">
                <div class="container has-text-centered">
                    <div class="columns is-multiline">
                        <div class="column is-4">
                            <nav class="panel title-space">
                                <p class="panel-heading">
                                    Our Services
                                </p>
                                <a href="#" class="panel-block is-active" onclick="toggleTab('mobile-apps')">
                                    Mobile App Development
                                </a>
                                <a href="#" class="panel-block" onclick="toggleTab('portals')">
                                    Web Portals
                                </a>
                                <a href="#" class="panel-block" onclick="toggleTab('mobile-games')">
                                    Mobile Game Development
                                </a>
                                <a href="#" class="panel-block" onclick="toggleTab('software')">
                                    Custom Software Development
                                </a>
                                <a href="#" class="panel-block" onclick="toggleTab('consultancy')">
                                    IT Consultancy
                                </a>
                                <a href="#" class="panel-block" onclick="toggleTable('graphics')">
                                    Graphic Design
                                </a>
                            </nav>
                        </div>
                        <div class="column is-8">
                            <article class="media title-space mobile-app">
                                <div class="media-content">
                                    <figure class="image">
                                        <img src="img/bg.jpg" class="is-square" alt="">
                                        <div class="caption">
                                            <p class="title is-4">Mobile App</p>
                                            {{--<p class="subtitle">Something like a tag line of some sort</p>--}}
                                        </div>
                                    </figure>
                                    <div class="content title-space">
                                        <p>We develop iOS and Android mobile Applications, interface designing and mobile back-ends, provide integration and maintenance. We listen to what content and design you need, then we deliver in time at a minimum budget. </p>
                                        <p>Click on the button below to get a free project estimate by writing what you require from us and we will get in touch with you.</p>
                                        <div class="has-text-right">
                                            <button class="button is-primary is-medium">
                                            <span class="icon has-icons-left">
                                                <i class="fa fa-comments"></i>
                                            </span>
                                                <span>Get A quote</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="media title-space web-portal">
                                <div class="media-content">
                                    <figure class="image">
                                        <img src="img/bg.jpg" class="is-square" alt="">
                                        <div class="caption">
                                            <p class="title is-4">Mobile App</p>
                                            {{--<p class="subtitle">Something like a tag line of some sort</p>--}}
                                        </div>
                                    </figure>
                                    <div class="content title-space">
                                        <p>We develop iOS and Android mobile Applications, interface designing and mobile back-ends, provide integration and maintenance. We listen to what content and design you need, then we deliver in time at a minimum budget. </p>
                                        <p>Click on the button below to get a free project estimate by writing what you require from us and we will get in touch with you.</p>
                                        <div class="has-text-right">
                                            <button class="button is-primary is-medium">
                                            <span class="icon has-icons-left">
                                                <i class="fa fa-comments"></i>
                                            </span>
                                                <span>Get A quote</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="media title-space mobile-game">
                                <div class="media-content">
                                    <figure class="image">
                                        <img src="img/bg.jpg" class="is-square" alt="">
                                        <div class="caption">
                                            <p class="title is-4">Mobile App</p>
                                            {{--<p class="subtitle">Something like a tag line of some sort</p>--}}
                                        </div>
                                    </figure>
                                    <div class="content title-space">
                                        <p>We develop iOS and Android mobile Applications, interface designing and mobile back-ends, provide integration and maintenance. We listen to what content and design you need, then we deliver in time at a minimum budget. </p>
                                        <p>Click on the button below to get a free project estimate by writing what you require from us and we will get in touch with you.</p>
                                        <div class="has-text-right">
                                            <button class="button is-primary is-medium">
                                            <span class="icon has-icons-left">
                                                <i class="fa fa-comments"></i>
                                            </span>
                                                <span>Get A quote</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="media title-space software">
                                <div class="media-content">
                                    <figure class="image">
                                        <img src="img/bg.jpg" class="is-square" alt="">
                                        <div class="caption">
                                            <p class="title is-4">Mobile App</p>
                                            {{--<p class="subtitle">Something like a tag line of some sort</p>--}}
                                        </div>
                                    </figure>
                                    <div class="content title-space">
                                        <p>We develop iOS and Android mobile Applications, interface designing and mobile back-ends, provide integration and maintenance. We listen to what content and design you need, then we deliver in time at a minimum budget. </p>
                                        <p>Click on the button below to get a free project estimate by writing what you require from us and we will get in touch with you.</p>
                                        <div class="has-text-right">
                                            <button class="button is-primary is-medium">
                                            <span class="icon has-icons-left">
                                                <i class="fa fa-comments"></i>
                                            </span>
                                                <span>Get A quote</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="media title-space consultancy">
                                <div class="media-content">
                                    <figure class="image">
                                        <img src="img/bg.jpg" class="is-square" alt="">
                                        <div class="caption">
                                            <p class="title is-4">Mobile App</p>
                                            {{--<p class="subtitle">Something like a tag line of some sort</p>--}}
                                        </div>
                                    </figure>
                                    <div class="content title-space">
                                        <p>We develop iOS and Android mobile Applications, interface designing and mobile back-ends, provide integration and maintenance. We listen to what content and design you need, then we deliver in time at a minimum budget. </p>
                                        <p>Click on the button below to get a free project estimate by writing what you require from us and we will get in touch with you.</p>
                                        <div class="has-text-right">
                                            <button class="button is-primary is-medium">
                                            <span class="icon has-icons-left">
                                                <i class="fa fa-comments"></i>
                                            </span>
                                                <span>Get A quote</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="media title-space graphic">
                                <div class="media-content">
                                    <figure class="image">
                                        <img src="img/bg.jpg" class="is-square" alt="">
                                        <div class="caption">
                                            <p class="title is-4">Mobile App</p>
                                            {{--<p class="subtitle">Something like a tag line of some sort</p>--}}
                                        </div>
                                    </figure>
                                    <div class="content title-space">
                                        <p>We develop iOS and Android mobile Applications, interface designing and mobile back-ends, provide integration and maintenance. We listen to what content and design you need, then we deliver in time at a minimum budget. </p>
                                        <p>Click on the button below to get a free project estimate by writing what you require from us and we will get in touch with you.</p>
                                        <div class="has-text-right">
                                            <button class="button is-primary is-medium">
                                            <span class="icon has-icons-left">
                                                <i class="fa fa-comments"></i>
                                            </span>
                                                <span>Get A quote</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--begin footer-->
@endsection