<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Tujengetech | @yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ URL::asset('/css/bulma.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/slick.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/styles.css') }}">
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<!--begin nav-->
<nav class="nav has-shadow is-fixed">
    <div class="nav-left margin-left">
        <a class="nav-item" href="/">
            <img src="/img/logo.png" alt="Tujenge Technologies Limited">
        </a>
    </div>
    <!-- This "nav-toggle" hamburger menu is only visible on mobile -->
    <!-- You need JavaScript to toggle the "is-active" class on "nav-menu" -->
  <span class="nav-toggle">
    <span></span>
    <span></span>
    <span></span>
  </span>

    <!-- This "nav-menu" is hidden on mobile -->
    <!-- Add the modifier "is-active" to display it on mobile -->
    <div class="nav-right nav-menu">
        <a class="nav-item is-tab nav-font-size @if(Route::currentRouteName() == "home") is-active @endif" href="{{ route('home') }}">
            Home
        </a>
        <a class="nav-item is-tab nav-font-size @if(Route::currentRouteName() == "about") is-active @endif" href="{{ route('about') }}">
            About
        </a>
        <a class="nav-item is-tab nav-font-size @if(Route::currentRouteName() == "services" || Route::currentRouteName() == "mobile-apps" || Route::currentRouteName() == "portals" || Route::currentRouteName() == "software" || Route::currentRouteName() == "mobile-games" || Route::currentRouteName() == "networking" || Route::currentRouteName() == "graphics") is-active @endif" href="{{ route('services') }}">
            Services
        </a>
        <!-- <a class="nav-item is-tab nav-font-size @if(Route::currentRouteName() == "team") is-active @endif" href="{{ route('team') }}">
            Team
        </a> -->

        <div class="nav-item margin-right">
            <div class="field">
                <p class="control">
                    <a class="button is-primary @if(Route::currentRouteName() == "quote") is-outlined @endif" href="{{ route('quote') }}">
                        <span>Request Quote</span>
                        <span class="icon is-small">
                          <i class="fa fa-paper-plane-o"></i>
                        </span>
                    </a>
                </p>
            </div>
        </div>
    </div>
</nav>
<!--end nav-->
@yield("content")

<!--begin footer-->
<footer class="footer is-paddingless bg-gray">
    <div class="container padding-2rem">
        <div class="columns is-multiline">
            <div class="column is-4">
                <aside class="menu">
                    <ul class="menu-list none">
                        <li><img src="{{ URL::asset('img/logo_white.png') }}" alt="" class="is-128x128 footer-logo">
                            <ul>
                                <li><a href="/about">About TujengeTech</a></li>
                                <!-- <li><a href="/team">Management Team</a></li> -->
                            </ul>
                        </li>
                    </ul>
                </aside>
            </div>
            <div class="column is-4">
                <aside class="menu">
                    <ul class="menu-list none">
                        <li><a href="{{ route('services') }}" class="is-paddingless">Mobile App Development</a></li>
                        <li><a href="{{ route('portals') }}" class="is-paddingless">Web Portals</a></li>
                        <li><a href="{{ route('mobile-games') }}" class="is-paddingless">Mobile Games Development</a></li>
                        <li><a href="{{ route('software') }}" class="is-paddingless">Custom Software Development</a></li>
                        <li><a href="{{ route('networking') }}" class="is-paddingless">Networking</a></li>
                        <li><a href="{{ route('graphics') }}" class="is-paddingless">Graphic Design</a></li>
                    </ul>
                </aside>
            </div>
            <div class="column is-4">
                <aside class="menu">
                    <ul class="menu-list has-text-dark none">
                        <li>
                            <a href="tel:+255653478869"><i class="fa fa-phone"></i>&nbsp;+255 653 478 869</a></li>
                        </li>
                        <li>
                            <a href="mailto:info@tujengetech.co.tz"><i class="fa fa-envelope"></i>&nbsp;info@tujengetech.co.tz</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-map-marker"></i>&nbsp;COSTECH Building, 1<super>st</super> Floor <br>Sayansi, Dar Es Salaam.</a>
                        </li>
                    </ul>
                </aside>
            </div>
        </div>
        <hr>
        <div class="level has-text-dark">
            <div class="level-item white-text">
                &copy; 2017 Tujenge Technologies Limited.
            </div>
            <div class="level-item">
                <!--<p class="heading">Follow Us</p>-->
                <a href="https://www.instagram.com/tujenge_tech/?hl=en" target="_blank">
                    <span class="icon white-text">
                        <i class="fa fa-instagram"></i>
                    </span>
                </a>
                <a href="https://www.facebook.com/TujengeTech/?hc_ref=PAGES_TIMELINE" target="_blank">
                    <span class="icon white-text">
                        <i class="fa fa-facebook"></i>
                    </span>
                </a>
                <a href="https://twitter.com/TujengeTech" target="_blank">
                    <span class="icon white-text">
                        <i class="fa fa-twitter"></i>
                    </span>
                </a>
            </div>
            <div class="level-item">
                <a href="{{ route('quote') }}" class="button is-primary @if(Route::currentRouteName() == "quote") is-outlined @endif">Request Quote</a>
            </div>
        </div>
    </div>
</footer>
<!--end footer-->
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('js/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/plugins.js') }}"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
                function () {
                    (b[l].q = b[l].q || []).push(arguments)
                });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = 'https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>
