@extends('layouts.container')
@section('title', "The brains")
@section('content')
    <!--begin team section-->
    <section class="hero">
        <div class="hero-body">
            <div class="container">
                <h1 class="title title-space">Management Team</h1>
                <div class="columns is-multiline">
                    <div class="column is-4 is-offset-4">
                        <div class="box">
                            <article class="team has-text-centered">
                                <figure class="image is-128x128">
                                    <img src="http://bulma.io/images/placeholders/128x128.png">
                                </figure>
                                <br>
                                <p class="title">Joseph Singano</p>
                                <p class="subtitle">CEO</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium autem dolore non placeat quae, quam qui sit veniam vitae voluptatem. Accusamus doloribus explicabo incidunt iure nemo quas velit voluptates voluptatibus?</p>
                            </article>
                        </div>
                    </div>
                </div>
                <div class="columns is-multiline">
                    <div class="column is-4">
                        <div class="box">
                            <article class="team has-text-centered">
                                <figure class="image is-128x128">
                                    <img src="http://bulma.io/images/placeholders/128x128.png">
                                </figure>
                                <br>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium autem dolore non placeat quae, quam qui sit veniam vitae voluptatem. Accusamus doloribus explicabo incidunt iure nemo quas velit voluptates voluptatibus?</p>
                            </article>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="box">
                            <article class="team has-text-centered">
                                <figure class="image is-128x128">
                                    <img src="http://bulma.io/images/placeholders/128x128.png">
                                </figure>
                                <br>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium autem dolore non placeat quae, quam qui sit veniam vitae voluptatem. Accusamus doloribus explicabo incidunt iure nemo quas velit voluptates voluptatibus?</p>
                            </article>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="box">
                            <article class="team has-text-centered">
                                <figure class="image is-128x128">
                                    <img src="http://bulma.io/images/placeholders/128x128.png">
                                </figure>
                                <br>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium autem dolore non placeat quae, quam qui sit veniam vitae voluptatem. Accusamus doloribus explicabo incidunt iure nemo quas velit voluptates voluptatibus?</p>
                            </article>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="box">
                            <article class="team has-text-centered">
                                <figure class="image is-128x128">
                                    <img src="http://bulma.io/images/placeholders/128x128.png">
                                </figure>
                                <br>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium autem dolore non placeat quae, quam qui sit veniam vitae voluptatem. Accusamus doloribus explicabo incidunt iure nemo quas velit voluptates voluptatibus?</p>
                            </article>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="box">
                            <article class="team has-text-centered">
                                <figure class="image is-128x128">
                                    <img src="http://bulma.io/images/placeholders/128x128.png">
                                </figure>
                                <br>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium autem dolore non placeat quae, quam qui sit veniam vitae voluptatem. Accusamus doloribus explicabo incidunt iure nemo quas velit voluptates voluptatibus?</p>
                            </article>
                        </div>
                    </div>
                    <div class="column is-4">
                        <div class="box">
                            <article class="team has-text-centered">
                                <figure class="image is-128x128">
                                    <img src="http://bulma.io/images/placeholders/128x128.png">
                                </figure>
                                <br>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium autem dolore non placeat quae, quam qui sit veniam vitae voluptatem. Accusamus doloribus explicabo incidunt iure nemo quas velit voluptates voluptatibus?</p>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end services section-->
@endsection