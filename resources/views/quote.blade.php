@extends("layouts.container")
@section('title', "Get your Free Quote")
@section("content")
    @if(session('notification'))
        @include('includes.success', ['notification' => session('notification')])
    @endif
    @if(session('failed'))
        @include('includes.fail', ['fail' => session('notification')])
    @endif
    <div class="column"></div>
    <div class="column"></div>
    <!--begin quote-->
    <section class="hero">
        <div class="hero-head">
            <div class="container">
                <div class="column is-8 is-offset-2">
                    <p class="title title-space">Get a Free Quote</p>
                    <p>Thank you for your interests in Tujenge Technologies Limited</p>
                    <br>
                    <p class="has-text-justify">We will be glad to answer all your questions as well as estimate any project of yours. Our specialists will process the request and we will get back to you within one business day to provide a valuble insight.</p>
                </div>
            </div>
        </div>
        <div class="hero-body">
            <div class="container">
                <form action="{{ route('hello-mail') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="columns">
                        <div class="column is-8 is-offset-2">
                            <div class="columns is-multiline">
                                <div class="column is-half">
                                    <div class="field">
                                        <label class="label">Full Name</label>
                                        <p class="control">
                                            <input class="input" type="text" placeholder="Full Name *" name="name" required>
                                        </p>
                                    </div>
                                </div>
                                <div class="column is-half">
                                    <div class="field">
                                        <label class="label">Email</label>
                                        <p class="control">
                                            <input class="input" type="text" placeholder="email@domain.com *" name="email" required>
                                        </p>
                                    </div>
                                </div>
                                <div class="column is-half">
                                    <div class="field">
                                        <label class="label">Company</label>
                                        <p class="control">
                                            <input class="input" type="text" placeholder="Company *" required name="company">
                                        </p>
                                    </div>
                                </div>
                                <div class="column is-half">
                                    <div class="field">
                                        <label class="label">Phone</label>
                                        <p class="control">
                                            <input class="input" type="text" placeholder="Phone *" required name="phone">
                                        </p>
                                    </div>
                                </div>
                                <div class="column is-12">
                                    <div class="field">
                                        <p class="control">
                                            <textarea class="textarea" name="message" placeholder="How can we help you? *(Please enter no more than 500 characters)" maxlength="500" required></textarea>
                                        </p>
                                    </div>
                                </div>
                                <div class="column is-12">
                                    <div class="field">
                                        <label class="label">Add Attachment</label>
                                        <p class="control">
                                            <input class="input" type="file" name="file">
                                        </p>
                                    </div>
                                </div>
                                <div class="column">
                                    <p class=" has-text-centered">
                                        <button type="submit" class="button is-large is-primary is-outlined">SEND REQUEST</button>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </section>
    <!--end quote-->
@endsection