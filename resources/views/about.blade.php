@extends('layouts.container')
@section('title', "The things we do in a nutshell")
@section('content')
    <!--begin landing page section-->
    <section class="hero is-fullheight">
        <div class="container is-fluid">
            <div class="hero-body">
                <div class="container has-text-centered">
                    <div class="columns is-multiline">
                        <div class="column is-4">
                            <nav class="panel title-space">
                                <p class="panel-heading">
                                    Our Services
                                </p>
                                <a href="{{ route('services') }}" class="panel-block">
                                    Mobile App Development
                                </a>
                                <a href="{{ route('portals') }}" class="panel-block">
                                    Web Portals
                                </a>
                                <a href="{{ route('mobile-games') }}" class="panel-block">
                                    Mobile Game Development
                                </a>
                                <a href="{{ route('software') }}" class="panel-block">
                                    Custom Software Development
                                </a>
                                <a href="{{ route('networking') }}" class="panel-block">
                                    Networking
                                </a>
                                <a href="{{ route('graphics') }}" class="panel-block">
                                    Graphic Design
                                </a>
                            </nav>
                        </div>
                        <div class="column is-8">
                            <article class="media title-space mobile-app">
                                <div class="media-content has-text-justify">
                            <p class="title text-uppercase has-text-left">About Tujenge Technologies Limited</p>
                                    <figure class="image">
                                        <img src="{{ URL::asset('img/bg.jpg') }}" class="is-square" alt="">
                                        <!-- <div class="caption">
                                            <p class="title is-4">About Tujenge Technologies Limited</p>
                                        </div> -->
                                    </figure>
                                    <div class="content title-space">
                                    
                <div class="columns is-multiline">
                    <div class="column">
                        <article>
                            <p class="title is-4">OVERVIEW</p>
                            Tujenge Technologies Limited is an ICT-based company, incorporated under the Company’s ACT of 2002 of The United Republic of Tanzania, whose offices are at COSTECH Building, 1st Floor (Wing A), Kijitonyama, Ali Hassan Mwinyi Road, P.O.Box 4302, Dar es salaam, Tanzania.
                        </article>
                        <br>
                        <article>
                            <p class="title is-4">OUR MISSION</p>
                            To empower Local communities via digital inclusion through designing and developing compatible digital soliutions that address community challenges.
                        </article>
                        <br>
                        <article>
                            <p class="title is-4">OUR VISION</p>
                            To be the leading and trusted Tech Company in developing and delivering quality digital solutions that are compatible in addressing communities challenges
                        </article>
                        <br>
                        <article>
                            <p class="title is-4 text-uppercase">Our Priority</p>
                            Our priorityis to serve and deliver to our clients Quality, Simple and Reliable solutions.
                        </article>
                        <br>
                        <a href="{{ route('quote') }}" class="button is-primary">Contact Us Now</a>
                    </div>
                </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--begin footer-->
@endsection