@extends("layouts.container")
@section('title', "Your Go to team for smart solutions")
@section("content")
    @if(session('notification'))
        @include('includes.success', ['notification' => session('notification')])
    @endif
    @if(session('failed'))
        @include('includes.fail', ['fail' => session('notification')])
    @endif
<!--begin landing page section-->
<section class="hero bg-1 is-fullheight">
  <div class="hero-body">
    <div class="container">
      <h1 class="title is-1 has-text-white">
        Professional Software Developers
      </h1>
      <h2 class="subtitle is-4 has-text-white">
        We serve and deliver to our clients; Quality, Simple and Compartible Solutions.
      </h2>
      <a class="button is-large is-primary title-space" href="{{ route('services') }}">
      <span>Get A Free Quote</span>
      <span class="icon">
          <fa class="fa fa-paper-plane"></fa>
      </span>
      </a>
    </div>
  </div>
</section>
<!-- <section class="hero is-fullheight is-primary bg-landing">
    <div class="container is-fluid,">
        <div class="hero-body">
            <div class="container">
                <h1 class="title text-shadow slogan,">Professional Software Developers</h1>
                <div class="level title-space">
                    <div class="level-item has-text-centered">
                        <div>
                        <span class="icon is-xl bg-circle">
                            <i class="fa fa-book"></i>
                        </span>
                            <p class="title title-space">Education</p>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                        <span class="icon is-xl bg-circle">
                            <i class="fa fa-magic"></i>
                        </span>
                            <p class="title title-space">eGovernance</p>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <span class="icon is-xl bg-circle">
                                <i class="fa fa-money"></i>
                            </span>
                            <p class="title title-space">Financial Inclusions</p>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <span class="icon is-xl bg-circle">
                                <i class="fa fa-shopping-bag"></i>
                            </span>
                            <p class="title title-space">Retail</p>
                        </div>
                    </div>
                </div>
                <a class="button is-large is-primary is-outlined title-space" href="{{ route('services') }}">MORE INDUSTRIES</a>
            </div>
        </div>
    </div>
</section> -->
<!--end landing page section-->
<!--begin services section-->
<section class="hero bg-light-gray">
    <div class="hero-body">
        <div class="container">
            <div class="columns is-multiline">
                <div class="column is-4">
                    <div class="card has-text-centered">
                        <div class="card-content">
                            <span class="icon is-large bg-circle padding-2rem has-text-light">
                                <i class="fa fa-mobile"></i>
                            </span>
                            <p class="subtitle">
                                Mobile App Development
                            </p>
                            <p>We develop iOS, Android, WP, Cordova and Xamarin apps, mobile back-ends, provide integration and maintenance</p>
                            <br>
                        </div>
                        <footer class="card-footer">
                            <div class="card-footer-item">
                                <a href="{{ route('services') }}">See More</a>
                            </div>
                        </footer>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="card has-text-centered">
                        <div class="card-content">
                            <span class="icon is-large bg-circle padding-2rem has-text-light">
                                <i class="fa fa-code"></i>
                            </span>
                            <p class="subtitle">
                                Web Portals
                            </p>
                            <p>We provide Web designing (UI/UX), development, integration, security audit and maintenance. We also re-design the existing web portal look and feel (UI/UX).</p>
                        </div>
                        <footer class="card-footer">
                            <div class="card-footer-item">
                                <a href="{{ route('portals') }}">See More</a>
                            </div>
                        </footer>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="card has-text-centered">
                        <div class="card-content">
                            <span class="icon is-large bg-circle padding-2rem has-text-light">
                                <i class="fa fa-gamepad"></i>
                            </span>
                            <p class="subtitle">
                                Mobile Games Development
                            </p>
                            <p>Mobile game concept designing, game logic designing and game animated characters designing, game creation and mobile back-ends, integration and maintenance.</p>
                        </div>
                        <footer class="card-footer">
                            <div class="card-footer-item">
                                <a href="{{ route('mobile-games') }}">See More</a>
                            </div>
                        </footer>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="card has-text-centered">
                        <div class="card-content margin-bottom">
                            <span class="icon is-large bg-circle padding-2rem has-text-light">
                                <i class="fa fa-file-code-o"></i>
                            </span>
                            <p class="subtitle">
                                Custom Software Development
                            </p>
                            <p>We offer full-cycle development services for web applications, mobile applications and desktop applications basing on your business needs. </p>
                        </div>
                        <footer class="card-footer">
                            <div class="card-footer-item">
                                <a href="{{ route('software') }}">See More</a>
                            </div>
                        </footer>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="card has-text-centered">
                        <div class="card-content">
                            <span class="icon is-large bg-circle padding-2rem has-text-light">
                                <i class="fa fa-handshake-o"></i>
                            </span>
                            <p class="subtitle">
                                Networking
                            </p>
                            <p>We also provide Network installation and configuration services (WIRELESS, LAN-1, CCTV Cameras), Maintenance and repair of office equipments (Computers, Printers, Scanners).</p>
                        </div>
                        <footer class="card-footer">
                            <div class="card-footer-item">
                                <a href="{{ route('networking') }}">See More</a>
                            </div>
                        </footer>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="card has-text-centered">
                        <div class="card-content">
                            <span class="icon is-large bg-circle padding-2rem has-text-light">
                                <i class="fa fa-deviantart"></i>
                            </span>
                            <p class="subtitle">
                                Graphic Designing
                            </p>
                            <p>We provide wide range of items designing from motion graphics, animations, logo designing, banner designing, business cards designing, poster designing and other designs as required by clients. </p>
                        </div>
                        <footer class="card-footer">
                            <div class="card-footer-item">
                                <a href="{{ route('graphics') }}">See More</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end services section-->
<!--begin track record section-->
<section class="hero is-fullheight">
    <div class="hero-head">
        <div class="container has-text-centered">
            <p class="title title-space">Our Track Record</p>
            <!-- <div class="columns">
                <div class="column is-8 is-offset-4">
                    <div class="columns is-multiline">
                        <div class="column is-3">
                            <img src="img/clutch-topdevelopers.svg" alt="">
                        </div>
                        <div class="column is-3">
                            <img src="img/clutch-topdevelopers.svg" alt="">
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
    <div class="hero-body">
        <div class="container">
            <div class="columns is-multiline">
                <div class="column is-4">
                    <div class="card has-text-centered padding-2rem gradient-1">
                        <div class="card-content">
                            <p class="title white-text">
                                10+
                            </p>
                            <p class="subtitle white-text">Projects</p>
                            <p>We have four Software projects in the market including the University communications system (DAAS), EthanMan mobile Game, Maduka App, i-Learn East Africa online learning website, and others work-in progress with the National Economic Empowerment Council. With four Networking projects in Mtwara region with four different companies. </p>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="card has-text-centered padding-2rem gradient-2">
                        <div class="card-content">
                            <p class="title white-text">
                                12+
                            </p>
                            <p class="subtitle white-text">Partners</p>
                            <p>With more than 10 reputable partners ranging from Government Institutions and Private institutions, we work with the Commission of Science and Technology (COSTECH), DTBi, National Economic Empowerment Council (NEEC), The University of Dar es salaam, UCC, VSDA, Frontline Porter Noveli, i-Learn East Africa.</p>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="card has-text-centered padding-2rem gradient-3">
                        <div class="card-content">
                            <p class="title white-text">
                                10+
                            </p>
                            <p class="subtitle white-text">Clients</p>
                            <p>With more than 10 clients ranging from Government Institutions and Private institutions, we have worked with the National Economic Empowerment Council, The University of Dar es salaam, Frontline Porter Noveli, i-Learn East Africa, Naliendele Agricultural Research Institute, Mtwara, NAF Beach Hotel, Mtwara, Zanzibar Insurance, Southern CTV Network.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end track record section-->
<!--begin testimonials-->
<section class="hero gradient-4">
    <div class="hero-head">
        <div class="container has-text-centered">
            <h1 class="title title-space white-text">Testimonials</h1>
        </div>
    </div>
    <div class="hero-body has-text-centered">
        <div class="container">
        <div class="columns">
            <div class="column is-10 is-offset-1 testimonial">
                <article>
                <h1 class="title white-text">iLearn East Africa LTD</h1>
                <p class="subtitle custom white-text">The team has been delivering results within budget and time. I’m absolutely satisfied with the quality of their services, their development skills and responsibility as well as the way they manage communication with us. We received the best experience working with the team in re-designing i-Learn East Africa Online Learning website. I fully recommend Tujenge Tech as a reliable IT partner!</p>
                <strong class="white-text">By Ms. Noelah Bomani – MD, i-Learn East Africa Limited</strong>
                </article>
                <article>
                    <h1 class="title white-text">Frontline Porter Noveli</h1>
                    <p class="subtitle custom white-text">We appreciate their proactive approach and ability to suggest improvements to a prospective solution on both architectural and business levels. We know we can always rely on Tujenge Tech’s various competencies when our clients require quality software which would facilitate their business success.</p>
                    <strong class="white-text">By Ms. Irene Kiwia – CEO, Frontline Porter Noveli</strong>
                </article>
                <article>
                    <h1 class="title white-text">University of Dar Es Salaam</h1>
                    <p class="subtitle custom white-text">Tujenge Tech is a fast emerging Tech company in Tanzania, the solutions they provide are compatible and relevant with the target environment. The university Daily Academic Access System (DAAS) for students, academic instructors and Administrators is indeed one of its kind in the whole learning experience within most of the Higher Learning institutions in Tanzania. I fully recommend Tujenge Tech as a reliable IT partner!</p>
                    <strong class="white-text">By Dr. Gladness Salema – UDIEC Entrepreneurship Coordinator, UDSM</strong>
                    <br>
                </article>  
            </div>
        </div>
        </div>
    </div>
</section>
<!--end testimonials-->
<!--begin partners section-->
<section class="hero">
    <div class="hero-head">
        <div class="container has-text-centered">
            <p class="title title-space">Our Partners</p>
        </div>
    </div>
    <div class="hero-body">
        <div class="container">
            <!-- <div class="columns"> -->
                <!-- <div class="columns"> -->
            <div class="logos">
                <div class="partner-logo">
                    <img src="{{ URL::asset('/img/1.jpg') }}" alt="">
                </div>
                <div class="partner-logo">
                    <img src="{{ URL::asset('/img/2.jpg') }}" alt="">
                </div>
            </div>
                {{--<div class="level logos">--}}
                {{--<div class="level-item">--}}
                    {{--<img src="{{ URL::asset('/img/1.jpg') }}" class="partner-logo" alt="">--}}
                {{--</div>--}}
                {{--<div class="level-item">--}}
                    {{--<img src="{{ URL::asset('/img/2.png') }}" class="partner-logo" alt="">--}}
                {{--</div>--}}
                {{--<div class="level-item">--}}
                    {{--<img src="{{ URL::asset('/img/3.png') }}" class="partner-logo" alt="">--}}
                {{--</div>--}}
            <!-- </div> -->
            <!-- </div> -->
        </div>
    </div>
</section>
<!--end partners section-->
<!--begin quote-->
<section class="hero bg-light-gray">
    <div class="hero-head">
        <div class="container has-text-centered">
            <p class="title title-space">Get a Free Quote</p>
        </div>
    </div>
    <div class="hero-body">
        <div class="container">
            <form action="{{ route('hello-mail') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="columns">
                    <div class="column is-8 is-offset-2">
                        <div class="columns is-multiline">
                            <div class="column is-half">
                                <div class="field">
                                    <label class="label">Full Name</label>
                                    <p class="control">
                                        <input class="input" type="text" placeholder="Full Name *" name="name" required>
                                    </p>
                                </div>
                            </div>
                            <div class="column is-half">
                                <div class="field">
                                    <label class="label">Email</label>
                                    <p class="control">
                                        <input class="input" type="text" placeholder="email@domain.com *" name="email" required>
                                    </p>
                                </div>
                            </div>
                            <div class="column is-half">
                                <div class="field">
                                    <label class="label">Company</label>
                                    <p class="control">
                                        <input class="input" type="text" placeholder="Company *" name="company" required>
                                    </p>
                                </div>
                            </div>
                            <div class="column is-half">
                                <div class="field">
                                    <label class="label">Phone</label>
                                    <p class="control">
                                        <input class="input" type="tel" placeholder="Phone *" name="phone"required>
                                    </p>
                                </div>
                            </div>
                            <div class="column is-12">
                                <div class="field">
                                    <p class="control">
                                        <textarea class="textarea" placeholder="How can we help you? *(Please enter no more than 500 characters)" maxlength="500" name="message" required></textarea>
                                    </p>
                                </div>
                            </div>
                            <div class="column is-12">
                                <div class="field">
                                    <label class="label">Add Attachment</label>
                                    <p class="control">
                                        <input class="input" type="file" name="file">
                                    </p>
                                </div>
                            </div>
                            <div class="column">
                                <p class=" has-text-centered">
                                    <button type="submit" class="button is-large is-primary is-outlined">SEND REQUEST</button>
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</section>
<!--end quote-->
@endsection