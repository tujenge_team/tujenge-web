<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\HelloMail;

class ClientHello extends Mailable
{
    use Queueable, SerializesModels;
    public $hello;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    // public function __construct()
    public function __construct(HelloMail $hello)
    {
        $this->hello = $hello;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.hello');//->attach(asset("storage/$this->hello->file"));
    }
}
