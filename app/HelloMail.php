<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HelloMail extends Model
{
    protected $fillable = ['email', 'name', 'message', 'phone', 'company', 'file'];
}
