<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ClientHello;
use Illuminate\Http\File;

class HelloMailController extends Controller
{
    public function index(Request $request) {
    	$hello = new \App\HelloMail;
    	$hello->name = $request->name;
    	$hello->email = $request->email;
    	$hello->company = $request->company;
    	$hello->phone = $request->phone;
    	$hello->message = $request->message;


    	//file
    	if ($request->hasFile('file')) {
    		$hello->file = \Storage::putFile('public/hello-files', new File($request->file));
    	} else {
			$hello->file = "";
		}

    	if($hello->save()) {
	    	\Mail::to('web@tujengetech.co.tz')->send(new ClientHello($hello));
	    	return back()->with('notification', 'Your email has been sent, thank you');	
    	}
    	return back()->with('failed', 'Something didn\'t go right please resend');	
    }
}
