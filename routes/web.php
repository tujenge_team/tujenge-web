<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
//    return view('welcome');
})->name('home');

//Route::get('/services', function () {
//    return view('services');
//});


Route::get('team', function (){
    return view('team');
})->name('team');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/services/mobile-app', function () {
    return view('services.mobile-apps');
})->name('services');

Route::get('/services/mobile-game', function () {
    return view('services.mobile-games');
})->name('mobile-games');

Route::get('/services/software', function () {
    return view('services.softwares');
})->name('software');

Route::get('/services/portals', function () {
    return view('services.portals');
})->name('portals');

Route::get('/services/networking', function () {
    return view('services.networking');
})->name('networking');

Route::get('/services/graphics', function () {
    return view('services.graphics');
})->name('graphics');

Route::get('/quote', function () {
    return view('quote');
})->name('quote');

Route::post('/hello', 'HelloMailController@index')->name("hello-mail");